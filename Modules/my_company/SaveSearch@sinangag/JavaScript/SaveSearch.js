define('SaveSearch',
    ['SaveSearch.Router',
        'underscore'
    ]
,   function (
        Router,
        _
    ) {
    'use strict';

    return {
        MenuItems: {
            id: 'savesearch',
            name: _('Save Search List').translate(),
            url: 'savesearchlist',
            index: 0
        },

        mountToApp: function (application)
        {
            return new Router(application);
        }
    };
});
