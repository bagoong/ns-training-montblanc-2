define('SaveSearch.Model'
,	[	'Backbone'
	]
,	function (
		Backbone
	) {
	'use strict';

	return Backbone.Model.extend({
        urlRoot: 'services/SaveSearch.Service.ss'

	});
});
