define(
    'SaveSearch.List.View'
,   [   'savesearch_list.tpl'
    ,   'Backbone'
    ,   'Backbone.CompositeView'
    ,   'Backbone.CollectionView'
    ,   'RecordViews.View'
    ]
,   function (
        savesearch_list_tpl
    ,   Backbone
    ,   BackboneCompositeView
    ,   BackboneCollectionView
    ,   RecordViewsView
    )
{
    'use strict';

    return Backbone.View.extend({

        template: savesearch_list_tpl

    ,   attributes: { 'class': 'SaveSearchListView' }

    ,   title: _('Save Search List').translate()

    ,   getContext: function ()
        {
            return {
                pageHeader: _('SaveSearch Test').translate()
            //    numberWidgets: 'this.collection.length'
            //,   collection: 'this.collection'
            };
        }
    });
});
