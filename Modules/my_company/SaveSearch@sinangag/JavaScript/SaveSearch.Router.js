define('SaveSearch.Router',
    ['SaveSearch.List.View',
        'Profile.Model',
        'Backbone'
    ],
    function (
        SaveSearchListView,
        ProfileModel,
        Backbone
    ) {
        'use strict';

        return Backbone.Router.extend({

            routes: {
                'savesearchlist': 'savesearchList'
            },

            initialize: function (application) {
                this.application = application;
                this.collection = ProfileModel.getInstance().get('savesearch');
            },

            savesearchList: function () {
                console.log('savesearchlist function');

                var view = new SaveSearchListView({
                application: this.application
            ,   collection: this.collection
            });

            // On any change to collection: automatically refresh display of widgets
            view.collection.on('reset destroy change add', function ()
            {
                


                /*
                var model = arguments[0];               
                var collection = this.collection;
                
                var widget, shape;
                for (var i=0; i < collection.models.length; i++){
                    widget = collection.models[i];
                    if (widget.get('internalid') == model.get('internalid')) {
                        for (var j=0; j < SC.ENVIRONMENT.WIDGET_SHAPES.length; j++){
                            shape = SC.ENVIRONMENT.WIDGET_SHAPES[j];
                            if (shape.internalid == model.get('shape')){
                                widget.set('shape', shape.name);
                            }
                        }
                    }
                }
                */
                var currentView = this.application.getLayout().currentView;

                if (currentView instanceof SaveSearchListView || currentView instanceof SaveSearchListView)
                {
                    this.savesearchList();
                }

            }, this);           

            view.showContent();
            
            }
        });
    });
