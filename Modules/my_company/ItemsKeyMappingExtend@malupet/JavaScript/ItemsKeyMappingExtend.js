define('ItemsKeyMappingExtend', ['ItemsKeyMapping',
    'underscore',
    'SC.Configuration'
], function ItemImages(
    ItemsKeyMapping,
    _,
    Configuration
) {
    'use strict';

    function extendKeyMapping() {
        Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};
        _.extend(Configuration.itemKeyMapping, {
            _salesdescription: function (item) {
             console.log(item);
            // return item.get('salesdescription');
                var childs = item.getSelectedMatrixChilds();
                var salesdescription = item.get('salesdescription') || '';

                if (childs && childs.length === 1) {
                    salesdescription = childs[0].get('salesdescription') || salesdescription;
                }

                return salesdescription;
            }

        ,   _priceShedule: function (item) {

                var prices = item.get('_priceDetails');
                console.log(item);
                console.log(prices);
            //    return item.get('_priceDetails');
                
            }
        });
    }
	   return {
        mountToApp: function mountToApp(application) {
            extendKeyMapping();
        }
    };
});
