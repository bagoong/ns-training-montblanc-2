define(
	'ItemDetails.View.ShowSalesDescMatrix'
,	[
		'ItemDetails.View'
	,	'Profile.Model'
	]
,	function (
		ItemDetailsView
	,	ProfileModel
	)
{
	'use strict';

	var colapsibles_states = {};

	return _.extend(ItemDetailsView.prototype,{

	getContext: function ()
		{
			var model = this.model
			,	thumbnail = model.get('_images', true)[0] || model.get('_thumbnail')
			,	selected_options = model.getSelectedOptions()

			,	quantity = model.get('quantity')
			,	min_quantity = model.get('_minimumQuantity', true)
			,	min_disabled = false;


			if (model.get('quantity') <= model.get('_minimumQuantity', true))
			{
				// TODO: resolve error with dependency circular.
				if (require('LiveOrder.Model').loadCart().state() === 'resolved')
				{
					// TODO: resolve error with dependency circular.
					var itemCart = SC.Utils.findItemInCart(model, require('LiveOrder.Model').getInstance())
					,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + model.get('quantity')) <= model.get('_minimumQuantity', true))
					{
						min_disabled = true;
					}
				}
				else
				{
					min_disabled = false;
				}
			}

			//@class ItemDetails.View.Context
			return {
				//@property {ItemDetails.Model} model
				model: model
				//@property {Boolean} isPriceEnabled
			,	isPriceEnabled: !ProfileModel.getInstance().hidePrices()
				//@property {Array<ItemDetailsField>} details
			,	details: this.details
				//@property {Boolean} showDetails
			,	showDetails: this.details.length > 0
				//@property {Boolean} isItemProperlyConfigured
			,	isItemProperlyConfigured: model.isProperlyConfigured()
				//@property {Boolean} showQuantity
			,	showQuantity: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showRequiredReference
			,	showRequiredReference: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showSelectOptionifOutOfStock
			,	showSelectOptionMessage : !model.isSelectionComplete() && model.get('_itemType') !== 'GiftCert'
				//@property {Boolean} showMinimumQuantity
			,	showMinimumQuantity: !! min_quantity && min_quantity > 1
				//@property {Boolean} isReadyForCart
			,	isReadyForCart: model.isReadyForCart()
				//@property {Boolean} showReviews
			,	showReviews: this.reviews_enabled
				//@property {String} itemPropSku
			,	itemPropSku: '<span itemprop="sku">' + model.get('_sku', true) + '</span>'
				//@property {String} item_url
			,	item_url : model.get('_url') + model.getQueryString()
				//@property {String} thumbnailUrl
			,	thumbnailUrl : this.options.application.resizeImage(thumbnail.url, 'main')
				//@property {String} thumbnailAlt
			,	thumbnailAlt : thumbnail.altimagetext
				//@property {String} sku
			,	sku : model.get('_sku', true)
				//@property {Boolean} isMinQuantityOne
			,	isMinQuantityOne : model.get('_minimumQuantity', true) === 1
				//@property {Number} minQuantity
			,	minQuantity : min_quantity
				//@property {Number} quantity
			,	quantity : quantity
				//@property {Boolean} isMinusButtonDisabled
			,	isMinusButtonDisabled: min_disabled || model.get('quantity') === 1
				//@property {Boolean} hasCartItem
			,	hasCartItem : !!model.cartItemId
				//@property {Array} selectedOptions
			,	selectedOptions: selected_options
				//@property {Boolean} hasSelectedOptions
			,	hasSelectedOptions: !!selected_options.length
				//@property {Boolean} hasAvailableOptions
			,	hasAvailableOptions: !!model.getPosibleOptions().length
				//@property {Boolean} isReadyForWishList
			,	isReadyForWishList: model.isReadyForWishList()

			,	salesdescription: model.get('_salesdescription', true)
			};
		}
	});
});

//@class ItemDetails.View.Initialize.Parameters
//@property {ItemDetails.Model} model
//@property {String} baseUrl
//@property {ApplicationSkeleton} application
