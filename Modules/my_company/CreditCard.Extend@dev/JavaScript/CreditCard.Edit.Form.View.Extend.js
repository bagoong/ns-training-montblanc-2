/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CreditCard
define('CreditCard.Edit.Form.View.Extend'
,	[	
		'CreditCard.Edit.Form.View'
	,   'CreditCard.Edit.Form.SecurityCode.View'
	,	'creditcard_edit_form.tpl'	

	,	'SC.Configuration'
	,	'Backbone.CompositeView'
	,	'Backbone.FormView'
	,	'Backbone'
	,	'underscore'
	]
,	function (
		CreditCardEditFormView
	,	CreditCardEditFormSecurityCodeView
	,	creditcard_edit_form_tpl

	,	Configuration
	,	BackboneCompositeView
	,	BackboneFormView
	,	Backbone
	,	_
	)
{
	'use strict';

	// @class CreditCard.Edit.Form.View @extends Backbone.View
	return _.extend(CreditCardEditFormView.prototype,{

	
		initialize : function ()
		{
			if (this.options.showSecurityCodeForm)
			{
				this.model.set({ hasSecurityCode: true });
			}
			else
			{
				this.model.unset('hasSecurityCode', {silent: true});
			}


			if (!this.model.get('expmonth'))
			{
				this.model.set({ 'expmonth': this.options.currentMonth + '' }, { silent: true });
			}

			if (!this.model.get('expyear'))
			{
				this.model.set({ 'expyear': this.options.years[0] + '' }, { silent: true });
			}

			BackboneCompositeView.add(this);
			BackboneFormView.add(this);
		}

			//@class CreditCard.Form.View.Context
			return {
				//@property {Array<CreditCard.PaymentMetod>} paymentMethods
				paymentMethods: paymentMethods
				//@property {String} paymentMethodValue
			, 	paymentMethodValue: selected_payment_method ? selected_payment_method.key : ''
				//@property {String} ccnumber
			,	ccnumber: this.model.get('ccnumber')
				//@property {boolean} showPaymentSelector
			,	showPaymentSelector: this.model.isNew()	
				//@property {Boolean} isNew
			,	isNew: this.model.isNew()
				//@property {Array<Object>?} months			
			,	months: months
				//@property {Array<Object>?} years
			,	years: years
				//@property {Boolean} showDefaults

			,	showDefaults: !!this.options.showDefaults
				//@property {String} ccname
			,	ccname: this.model.get('ccname')
				//@property {Boolean} ccdefault
			,	ccdefault: this.model.get('ccdefault') === 'T'
				//@property {Boolean} showSecurityCode
			,	showSecurityCodeForm: !!this.options.showSecurityCodeForm

			};
		}
	});
});
